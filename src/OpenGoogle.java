
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class OpenGoogle
{
	WebDriver driver;
	
	@AfterTest
	public void packUp()
	{
		driver.close();
	}
	@Test
	public void openChrome()
	{
		String chromePath="C:\\Users\\amit.nale\\Desktop\\Software\\chromedriver_win32\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
		//driver = new FirefoxDriver();
		driver.get("https://www.google.co.in/");
	}
	

}
